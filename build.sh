#!/bin/bash

# Define the tag to use
TAG="imports/c7-rt/rtctl-1.13-2.el7"

git clone https://git.centos.org/git/centos-git-common.git
git clone https://git.centos.org/git/rpms/rtctl

cd rtctl
git checkout ${TAG} && ../centos-git-common/get_sources.sh -b c7-rt
rpmbuild --define "%_topdir `pwd`" --target noarch -ba SPECS/rtctl.spec
tree RPMS
